import sys
import os
import re
import codecs


def get_text(raw_text):
    text_lines = []
    with codecs.open(raw_text, 'r', 'utf-8') as f:
        for line in f:
            text_lines.append(line.strip())
    return text_lines


def main(zip_files):
    this_dir = os.getcwd()
    text_content = []
    pcm_files = []
    with codecs.open(zip_files, 'r') as f:
        for filename in f.readlines():
            base_filename = os.path.basename(filename.strip())
            dir_filename = os.path.dirname(filename.strip())
            print("raw file:{}, dir_filename:{}\n".format(
                base_filename, dir_filename))
            all_files = os.listdir(dir_filename)
            for file in all_files:
                curr_filename = '{}/{}'.format(dir_filename, file)
                if curr_filename.endswith('.scp'):
                    contents = get_text(curr_filename)
                    # append contents to the text_content array
                    for line in contents:
                        text_content.append(line)
                if curr_filename.endswith('pcm'):
                    pcm_files.append(curr_filename)


if __name__ == '__main__':
    input_zips = sys.argv[1]
    main(input_zips)
