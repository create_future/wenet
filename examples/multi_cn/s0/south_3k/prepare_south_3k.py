import sys
import os
import re
import codecs


def main(zip_files):
    this_dir = os.getcwd()
    with codecs.open(zip_files, 'r') as f:
        for filename in f.readlines():
            base_filename = os.path.basename(filename.strip())
            dir_filename = os.path.dirname(filename.strip())
            print("raw file:{}, dir_filename:{}\n".format(
                base_filename, dir_filename))
            print('cd {}'.format(dir_filename))
            # os.system('cd {}'.format(dir_filename))
            os.chdir(dir_filename)
            os.popen('unzip {}'.format(filename.strip()))
            # os.system('unzip {}'.format(filename.strip()))
            os.chdir(this_dir)


if __name__ == '__main__':
    input_zips = sys.argv[1]
    main(input_zips)
