import sys
import os
import codecs
import re


def wavscp2text(filename):
    with codecs.open(filename, 'r') as f:
        for line in f.readlines():
            key, _ = re.split(r'\s+', line.strip(), maxsplit=1)
            dest, _ = re.split(r'\.', line.strip(), maxsplit=1)
            if dest == 'pause':
                dest = 'ZAN4 TING2'
            if dest == 'play_previous':
                dest = 'SHANG4 YI1 GE4'
            if dest == 'play_next':
                dest = 'XIA4 YI2 GE4'
            if dest == 'play':
                dest = 'JI4 XU2'
            print('{}\t{}'.format(key, dest))


if __name__ == '__main__':
    wavscp2text(sys.argv[1])
