import sys
import os
import re
import codecs
import time
import logging
from datetime import datetime
from shutil import copyfile


def get_list(filename):
    badcases = []
    with codecs.open(filename, 'r') as f:
        for line in f:
            keys = re.split(r'\s+', line.strip(), maxsplit=1)
            badcases.append(keys[0])
    return badcases


def extract_badcases(badcase_list, filename_wav_scp):
    badcases = get_list(badcase_list)
    badcases = set(badcases)
    currentDateAndTime = datetime.now().strftime("%H-%M-%S")
    print("The current date and time is", currentDateAndTime)
    os.mkdir(currentDateAndTime, 755)
    with codecs.open(filename_wav_scp, 'r') as f:
        for line in f:
            key, wav_dir = re.split(r'\s+', line.strip(), maxsplit=1)
            if key in badcases:
                basename = os.path.basename(wav_dir)
                dest_filename = os.path.join(currentDateAndTime, basename)
                copyfile(wav_dir, dest_filename)
    os.system('zip -q -r {}.zip {}'.format(currentDateAndTime, currentDateAndTime))


if __name__ == '__main__':
    extract_badcases(sys.argv[1], sys.argv[2])
