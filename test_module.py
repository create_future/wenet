'''
Author: gaoxinglong
Date: 2022-11-18 11:33:52
LastEditTime: 2022-11-18 13:49:35
LastEditors: gaoxinglong
'''
from wenet.dataset_common.utils import list2tensor
from wenet.dataset.processor import pad_sequence
import numpy as np
import torch

from torch.nn.utils.rnn import pad_sequence


def padding_xamples():
    a = torch.ones(25, 300)
    b = torch.ones(22, 300)
    c = torch.ones(15, 300)
    print("size:{}\n".format(pad_sequence([a, b, c]).size()))
    # torch.Size([25, 3, 300])


def generate3d_list():
    data = np.random.random((3, 3, 3))
    return data.tolist()


def find_max_len():
    pass


if __name__ == "__main__":
    padding_xamples()
    vector = generate3d_list()
    std_vector = pad_sequence(vector, 0)
    tensor = torch.tensor(vector)
    print('done.')
