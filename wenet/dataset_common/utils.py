import copy
import numpy as np
import torch
import random
import kaldiio
import sys
import torchaudio
from typing import List
from wenet.dataset.processor import pad_sequence


def list2tensor(in_list, in_dim) -> torch.Tensor:
    pass


def get_data_from_dict(in_dict):
    if not isinstance(in_dict, dict):
        raise TypeError("{}".format(in_dict))
    xs = in_dict['xs']
    xlens = in_dict["xlens"]
    ys = in_dict["ys"]
    print("---------{},----------- {}---------\n".format(type(xs), type(ys)))
    # print("***************** raw xlens:{}\n".format(xlens))
    # print("+++++++++++++++++ raw xs:{}, raw ys:{}\n".format(xs, ys))
    xs_tensor_list = [torch.from_numpy(x) for x in xs]
    feats = pad_sequence(xs_tensor_list, True, 0.)
    feats_lengths = torch.tensor([x.size(0) for x in feats], dtype=torch.int32)

    # print("padding xs:{} , padding xlens:{}\n\n".format(
    #     feats, feats_lengths))

    ys_tensor_list = [torch.tensor(y, dtype=torch.int64) for y in ys]
    target = pad_sequence(ys_tensor_list, True, -1)
    target_lengths = torch.tensor([x.size(0) for x in target],
                                  dtype=torch.int32)

    print("-------------------target:{}, target_lengths:{}\n\n".format(
        target, target_lengths))
    # sys.exit(-1)
    return feats, target, feats_lengths, target_lengths


def tensor2np(x):
    if x is None:
        return x
    return x.cpu().detach().numpy()


def tensor2scalar(x):
    if isinstance(x, float):
        return x
    return x.cpu().detach().item()


def np2tensor(array, device=None):
    tensor = torch.from_numpy(array).to(device)
    return tensor


def pad_list(xs, pad_value=0., pad_left=False):
    """Convert list of Tensors to a single Tensor with padding.

    Args:
        xs (list): A list of length `[B]`, which contains Tensors of size `[T, input_size]`
        pad_value (float):
        pad_left (bool):
    Returns:
        xs_pad (FloatTensor): `[B, T, input_size]`

    """
    bs = len(xs)
    max_time = max(x.size(0) for x in xs)
    xs_pad = xs[0].new_zeros(bs, max_time, * xs[0].size()[1:]).fill_(pad_value)
    for b in range(bs):
        if len(xs[b]) == 0:
            continue
        if pad_left:
            xs_pad[b, -xs[b].size(0):] = xs[b]
        else:
            xs_pad[b, :xs[b].size(0)] = xs[b]
    return xs_pad


def set_batch_size(batch_size, dynamic_batching, num_replicas, df, offset):
    if not dynamic_batching:
        return batch_size

    min_xlen = df[offset:offset + 1]['xlen'].values[0]
    min_ylen = df[offset:offset + 1]['ylen'].values[0]

    if min_xlen <= 800:
        pass
    elif min_xlen <= 1600 or 80 < min_ylen <= 100:
        batch_size //= 2
    else:
        batch_size //= 8

    batch_size = batch_size // num_replicas * num_replicas
    batch_size = max(num_replicas, batch_size)
    # NOTE: ensure batch size>=1 for all replicas
    return batch_size


def sort_bucketing(df, batch_size, dynamic_batching,
                   num_replicas=1):
    """Bucket utterances in a sorted dataframe. This is also used for evaluation.

    Args:
        batch_size (int): size of mini-batch
        batch_size_type (str): type of batch size counting
        dynamic_batching (bool): change batch size dynamically in training
        num_replicas (int): number of replicas for distributed training
    Returns:
        indices_buckets (List[List]): bucketted utterances

    """
    indices_buckets = []  # list of list
    offset = 0
    indices_rest = list(df.index)
    while True:
        _batch_size = set_batch_size(
            batch_size, dynamic_batching, num_replicas, df, offset)

        indices = list(df[offset:offset + _batch_size].index)
        if len(indices) >= num_replicas:
            indices_buckets.append(indices)
        offset += len(indices)
        if offset >= len(df):
            break

    return indices_buckets


def shuffle_bucketing(df, batch_size, dynamic_batching,
                      seed=None, num_replicas=1):
    """Bucket utterances having a similar length and shuffle them for Transformer training.

    Args:
        batch_size (int): size of mini-batch
        batch_size_type (str): type of batch size counting
        dynamic_batching (bool): change batch size dynamically in training
        seed (int): seed for randomization
        num_replicas (int): number of replicas for distributed training
    Returns:
        indices_buckets (List[List]): bucketted utterances

    """
    indices_buckets = []  # list of list
    offset = 0
    while True:
        _batch_size = set_batch_size(
            batch_size, dynamic_batching, num_replicas, df, offset)

        indices = list(df[offset:offset + _batch_size].index)
        if len(indices) >= num_replicas:
            indices_buckets.append(indices)
        offset += len(indices)
        if offset >= len(df):
            break

    # shuffle buckets globally
    if seed is not None:
        random.seed(seed)
    random.shuffle(indices_buckets)
    return indices_buckets


def tensor2np(x):
    """Convert torch.Tensor to np.ndarray.

    Args:
        x (torch.Tensor):
    Returns:
        np.ndarray

    """
    if x is None:
        return x
    return x.cpu().detach().numpy()


def np2tensor(array, device=None):
    """Convert form np.ndarray to torch.Tensor.

    Args:
        array (np.ndarray): A tensor of any sizes
    Returns:
        tensor (torch.Tensor):

    """
    if not isinstance(array, np.ndarray):
        array = np.asarray(array)
    tensor = torch.from_numpy(array).to(device)
    return tensor


def pad_list(xs, pad_value=0., pad_left=False):
    """Convert list of Tensors to a single Tensor with padding.

    Args:
        xs (list): A list of length `[B]`, which contains Tensors of size `[T, input_size]`
        pad_value (float):
        pad_left (bool):
    Returns:
        xs_pad (FloatTensor): `[B, T, input_size]`

    """
    bs = len(xs)
    max_time = max(x.size(0) for x in xs)
    xs_pad = xs[0].new_zeros(bs, max_time, * xs[0].size()[1:]).fill_(pad_value)
    for b in range(bs):
        if len(xs[b]) == 0:
            continue
        if pad_left:
            xs_pad[b, -xs[b].size(0):] = xs[b]
        else:
            xs_pad[b, :xs[b].size(0)] = xs[b]
    return xs_pad


def hyp2text_wp(hyps, id2token):
    text = [id2token[hyp_id] for hyp_id in hyps]
    text = "".join(text)
    text = text.replace("▁", " ").strip(" ")
    return text


def hyp2text_char(hyps, id2token):
    text = [id2token[hyp_id] for hyp_id in hyps]
    text = " ".join(text)
    return text


def compute_wer(ref, hyp, normalize=False):
    """Compute Word Error Rate.

        [Reference]
            https://martin-thoma.com/word-error-rate-calculation/
    Args:
        ref (list): words in the reference transcript
        hyp (list): words in the predicted transcript
        normalize (bool, optional): if True, divide by the length of ref
    Returns:
        wer (float): Word Error Rate between ref and hyp
        n_sub (int): the number of substitution
        n_ins (int): the number of insertion
        n_del (int): the number of deletion

    """
    # Initialisation
    d = np.zeros((len(ref) + 1) * (len(hyp) + 1), dtype=np.uint16)
    d = d.reshape((len(ref) + 1, len(hyp) + 1))
    for i in range(len(ref) + 1):
        for j in range(len(hyp) + 1):
            if i == 0:
                d[0][j] = j
            elif j == 0:
                d[i][0] = i

    # Computation
    for i in range(1, len(ref) + 1):
        for j in range(1, len(hyp) + 1):
            if ref[i - 1] == hyp[j - 1]:
                d[i][j] = d[i - 1][j - 1]
            else:
                sub_tmp = d[i - 1][j - 1] + 1
                ins_tmp = d[i][j - 1] + 1
                del_tmp = d[i - 1][j] + 1
                d[i][j] = min(sub_tmp, ins_tmp, del_tmp)

    wer = d[len(ref)][len(hyp)]

    # Find out the manipulation steps
    x = len(ref)
    y = len(hyp)
    error_list = []
    while True:
        if x == 0 and y == 0:
            break
        else:
            if x > 0 and y > 0:
                if d[x][y] == d[x - 1][y - 1] and ref[x - 1] == hyp[y - 1]:
                    error_list.append("C")
                    x = x - 1
                    y = y - 1
                elif d[x][y] == d[x][y - 1] + 1:
                    error_list.append("I")
                    y = y - 1
                elif d[x][y] == d[x - 1][y - 1] + 1:
                    error_list.append("S")
                    x = x - 1
                    y = y - 1
                else:
                    error_list.append("D")
                    x = x - 1
            elif x == 0 and y > 0:
                if d[x][y] == d[x][y - 1] + 1:
                    error_list.append("I")
                    y = y - 1
                else:
                    error_list.append("D")
                    x = x - 1
            elif y == 0 and x > 0:
                error_list.append("D")
                x = x - 1
            else:
                raise ValueError

    n_sub = error_list.count("S")
    n_ins = error_list.count("I")
    n_del = error_list.count("D")
    n_cor = error_list.count("C")

    assert wer == (n_sub + n_ins + n_del)
    assert n_cor == (len(ref) - n_sub - n_del)

    if normalize:
        wer /= len(ref)

    return wer * 100, n_sub * 100, n_ins * 100, n_del * 100


class FeatExtractor:
    def __init__(self, cmvn):
        self.cmvn = kaldiio.load_mat(cmvn)
        stats_0, stats_1 = self.cmvn[0], self.cmvn[1]
        self.dim = 80
        count = stats_0[self.dim]
        norm = np.zeros([2, 80])
        for d in range(80):
            mean = stats_0[d] / count
            var = (stats_1[d] / count) - mean * mean
            floor = 1.0e-20
            if var < floor:
                print("Flooring cepstral variance from {} to {}".format(var, floor))
                var = floor
            scale = 1.0 / np.sqrt(var)
            if scale != scale or 1 / scale == 0.0:
                print("ERROR:NaN or infinity in cepstral mean/variance computation")
                return
            offset = -(mean * scale)
            norm[0][d] = offset
            norm[1][d] = scale
        self.norm = norm

    def apply_cmvn(self, feat):
        fream_num = feat.size(0)
        res = np.zeros([1, fream_num, self.dim])
        for t in range(fream_num):
            for d in range(self.dim):
                res[0][t][d] = feat[t][d] * self.norm[1][d] + self.norm[0][d]
        return res

    def extract_feat(self, audio_path):
        waveform, sample_rate = torchaudio.load(audio_path, normalize=False)
        fbank = torchaudio.compliance.kaldi.fbank(
            waveform.double(),
            num_mel_bins=80,
            frame_length=25,
            frame_shift=10,
            dither=0,
            htk_compat=True,
            window_type="hamming",
            sample_frequency=16000
        )
        fbank = self.apply_cmvn(fbank)
        return fbank
