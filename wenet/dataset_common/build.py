'''
Author: gaoxinglong
Date: 2022-11-12 12:11:57
LastEditTime: 2022-11-17 18:34:35
LastEditors: gaoxinglong
'''

from wenet.dataset_common.dataset import ASRDataset
from wenet.dataset_common.dataloader import ASRDataLoader
from wenet.dataset_common.sampler import ASRBatchSampler
from torch import tensor


def build_dataloader(args, tsv_path, batch_size, cmvn_path=None, num_workers=0, pin_memory=False, distributed=False, resume_epoch=0, device="cpu"):
    """_summary_

    Args:
        args (_type_): dict table
        tsv_path (_type_): input_tsv file
        batch_size (_type_): training batch_size
        cmvn_path (_type_, optional): cmvn file. Defaults to None.
        num_workers (int, optional): worker to run for training. Defaults to 0.
        pin_memory (bool, optional): _description_. Defaults to False.
        distributed (bool, optional): _description_. Defaults to False.
        resume_epoch (int, optional): _description_. Defaults to 0.
        device (str, optional): _description_. Defaults to "cpu".

    Returns:
        _type_: _description_
    """
    print("load data from : {}, batch_size: {}, cmvn_path:{}\n".format(
        tsv_path, batch_size, cmvn_path))
    dataset = ASRDataset(tsv_path=tsv_path,
                         cmvn_path=cmvn_path,
                         dict_path=args.dict,
                         wp_model=args.wp_model,
                         min_n_frames=args.min_n_frames,
                         max_n_frames=args.max_n_frames,
                         subsample_factor=args.subsample_factor)

    batch_sampler = ASRBatchSampler(dataset=dataset,
                                    distributed=distributed,
                                    batch_size=batch_size,
                                    dynamic_batching=args.dynamic_batching,
                                    shuffle_bucket=args.shuffle_bucket,
                                    resume_epoch=resume_epoch)

    dataloader = ASRDataLoader(dataset=dataset,
                               batch_sampler=batch_sampler,
                               sort_stop_epoch=args.sort_stop_epoch,
                               collate_fn=custom_collate_fn,
                               num_workers=num_workers,
                               pin_memory=pin_memory and distributed)

    return dataloader


def custom_collate_fn(data):
    tmp = {k: [data_i[k] for data_i in data] for k in data[0].keys()}

    return tmp
