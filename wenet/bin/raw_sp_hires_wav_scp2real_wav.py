'''
Author: create_future && gaoxinglong9999@sina.cn
Date: 2023-01-28 14:12:04
LastEditors: gaoxinglong && gaoxinglong9999@sina.cn
LastEditTime: 2023-01-28 20:56:01
FilePath: /wenet/wenet/bin/raw_sp_hires_wav_scp2real_wav.py
Description:
Copyright (c) 2023 by create_future gaoxinglong9999@sina.cn, All Rights Reserved.
'''
import sys
import os
import re
import codecs
import multiprocessing
from multiprocessing import Queue, Manager, Pool
from psutil import cpu_count
from typing import List, Dict, Optional, Union
import threading
# 14_3466_20170826171549 sox - -vol 1.2188413239781835 - t wav / home / share_ssd_data / 103 / audio_data / train / 14_3466_20170826171549.wav - t wav - |


def parse_cmd(cmd_line):
    if len(cmd_line) == 0:
        return None, None

    tokens = re.split(r"\s+", cmd_line)
    wav_path = ""
    for i in tokens:
        if re.match(r"\/", i):
            wav_path = i
            break
    base_filename = os.path.basename(wav_path)
    dir_wav_file = os.path.dirname(wav_path)
    main_filename, extend_filename = os.path.splitext(base_filename)
    sp_hires_main_filename = "{}_sp_hires.wav".format(main_filename)
    return "{}/{}".format(dir_wav_file, sp_hires_main_filename).strip("\n\r ")


def process_wave(wav_scp):
    with codecs.open(wav_scp, "r") as f:
        line = f.readline().strip()
        while line:
            tokens = re.split(r"\s+", line, 1)
            key, cmd_line = tokens[0], tokens[1]
            sp_hires_main_filename = parse_cmd(cmd_line)
            sp_hires_cmd = "{} cat >{}".format(
                cmd_line.strip("\r\n "), sp_hires_main_filename)
            print("cmd:{}\n".format(sp_hires_cmd))
            os.system(sp_hires_cmd)
            line = f.readline().strip()


def process_single_wave(thread_id, task_array: List, q):
    print("Processing single wave, thread_id:{}, num_tasks:{}".format(
        thread_id, len(task_array)))
    for line in task_array:
        tokens = re.split(r"\s+", line.strip(), 1)
        key, cmd_line = tokens[0], tokens[1]
        sp_hires_main_filename = parse_cmd(cmd_line)
        sp_hires_cmd = "{} cat >{}".format(
            cmd_line.strip("\r\n "), sp_hires_main_filename)
        print("cmd:{}\n".format(sp_hires_cmd))
        q.put("{}\t{}".format(key, sp_hires_main_filename))
        os.system(sp_hires_cmd)


def err_func(args):
    print("ending err_func:{}".format(args))


def parallel_process_wave(wav_scp):
    num_cpus = cpu_count()
    all_tasks: List = []
    with codecs.open(wav_scp, mode='r') as f:
        all_tasks = f.readlines()
    num_total_task = len(all_tasks)
    number_of_tasks = num_total_task // num_cpus + 1
    print("num_tasks:{}, cpus:{}\n".format(num_total_task, num_cpus))
    with Manager() as manager:
        p_pool = manager.Pool(num_cpus)
        q_shared = manager.Queue()
        for i in range(num_cpus):
            sub_tasks: List = []
            for line in all_tasks[i * number_of_tasks:(i + 1) * number_of_tasks]:
                sub_tasks.append(line)
            print("start thread : {} with number of tasks: {}\n".format(
                i, len(sub_tasks)))
            p_pool.apply_async(func=process_single_wave, args=(i,
                                                               sub_tasks, q_shared), callback=err_func)
        p_pool.close()
        p_pool.join()
    fout = codecs.open(wav_scp + ".sp_hires.wav.scp", mode='w')
    while not q_shared.empty():
        fout.write(q_shared.get())
    fout.close()


def parallel_process_threading(wav_scp):
    num_cpus = cpu_count()
    all_tasks: List = []
    with codecs.open(wav_scp, mode='r') as f:
        all_tasks = f.readlines()
    num_total_task = len(all_tasks)
    number_of_tasks = num_total_task // num_cpus + 1
    print("num_tasks:{}, cpus:{}\n".format(num_total_task, num_cpus))
    t = ['None'] * num_cpus
    q_shared = Queue()
    for i in range(num_cpus):
        sub_tasks: List = []
        for line in all_tasks[i * number_of_tasks:(i + 1) * number_of_tasks]:
            sub_tasks.append(line)
        print("start thread : {} with number of tasks: {}\n".format(
            i, len(sub_tasks)))
        t[i] = threading.Thread(
            target=process_single_wave, args=(i, sub_tasks, q_shared))
        t[i].start()
    for i in range(num_cpus):
        t[i].join()
    fout = codecs.open(wav_scp + ".sp_hires.wav.scp", mode='w')
    while not q_shared.empty():
        fout.write(q_shared.get())
    fout.close()


if __name__ == '__main__':
    # temp = "sox - -vol 1.2188413239781835 - t wav /home/share_ssd_data/103/audio_data/train/14_3466_20170826171549.wav - t wav - |"
    # x = parse_cmd(temp)
    wav_scp = sys.argv[1]
    parallel_process_threading(wav_scp=wav_scp)
