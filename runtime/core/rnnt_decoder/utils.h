/*
 * @Author: gaoxinglong
 * @Date: 2022-11-16 14:24:20
 * @LastEditTime: 2022-11-30 15:51:05
 * @LastEditors: gaoxinglong
 */
// Copyright (c) 2020 Mobvoi Inc (Binbin Zhang)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef UTILS_UTILS_H_
#define UTILS_UTILS_H_

#include <math.h>
#include <sstream>
#include <string>
#include <vector>

namespace wenet {

struct StreamingAsrConfig {
  std::string model_path{};
  std::string vocab_path{};
  std::string mvn_path{};
  int N_c;
  int N_l;
  int conv_context;
  int max_audio_second;
  int sample_rate;
  int output_dim;
  int input_dim{80};
  StreamingAsrConfig(const char *asr_config) { this->Read(asr_config); }
  StreamingAsrConfig()
      : N_c(40), N_l(40), conv_context(6), max_audio_second(15),
        sample_rate(16000), output_dim(6789), input_dim(80) {}
  void Read(const char *config_filename);
};
#define DISALLOW_COPY_AND_ASSIGN(Type)                                         \
  Type(const Type &) = delete;                                                 \
  Type &operator=(const Type &) = delete;

// void SplitString(const std::string& str, std::vector<std::string>* strs);

} // namespace wenet

struct ResultJson {
  int status_;
  std::string message_;
  std::string text_;
  int sample_num_;
  float audio_len_;

  ResultJson(int status, std::string message, std::string text, int sample_num,
             float audio_len)
      : status_(status), message_(message), text_(text),
        sample_num_(sample_num), audio_len_(audio_len) {}

  std::string GetJsonString() {
    // {
    //     "status": 0,
    //     "message": "success",
    //     "result":{
    //         "text":"",
    //         "sample_num":0,
    //         "audio_len":0.0,
    //     }
    // }
    std::string res = "{\"status\":" + std::to_string(status_) +
                      ", \"message\":\"" + message_ +
                      "\", \"result\":{\"text\":\"" + text_ +
                      "\", \"sample_num\":" + std::to_string(sample_num_) +
                      ", \"audio_len\":" + std::to_string(audio_len_) + "}}";
    return res;
  }
};

#endif // UTILS_UTILS_H_
