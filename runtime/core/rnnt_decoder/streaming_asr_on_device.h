/*
 * @Author: gaoxinglong
 * @Date: 2022-11-16 14:24:20
 * @LastEditTime: 2022-11-30 16:49:33
 * @LastEditors: gaoxinglong
 */
#ifndef STREAMING_ASR_ON_DEVICE_H_
#define STREAMING_ASR_ON_DEVICE_H_

#include "feature_pipeline.h"
#include "online_streaming.h"
#include "utils.h"
#include "wav.h"
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <sys/time.h>
#include <torch/script.h>
#include <unordered_map>
#include <vector>

#ifdef __ANDROID_API__
#include <android/log.h>
#define LOGD(...)                                                              \
  __android_log_print(ANDROID_LOG_DEBUG, "LOG_TAG", __VA_ARGS__);
#else
#define LOGD(...)                                                              \
  fprintf(stdout, "[%s:%d]:%s\n", __FILE__, __LINE__, ##__VA_ARGS__);
#endif
using namespace wenet;
int ReadJsonMvn(const char *filename,
                std::unordered_map<std::string, std::vector<float>> *mvn_stats);
std::string ReadFile(const char *filename);

class StreamingAsrOnDevice {
public:
  explicit StreamingAsrOnDevice(const StreamingAsrConfig &asr_config);

  int init();

  void reset();
  void rawPcm2Float(const char *buf, size_t len, std::vector<float> &result);
  std::string onRecieve(const float *wav, int len, bool is_last);
  std::string ProcessBlock(const float *wav, int len, bool is_last);

  inline std::string getHpy() { return hpy_; }
  const StreamingAsrConfig &getAsrConfig() const { return asr_config_; }

private:
  std::shared_ptr<OnlineStreaming> streaming_features_;
  const StreamingAsrConfig &asr_config_; //
  void applyCmvn(std::vector<float> &feat);
  std::vector<std::vector<float>> norm_;
  std::shared_ptr<wenet::FeaturePipeline> feature_pipeline_;

  int input_dim_{80};
  int N_c_{80};
  int N_l_{-1};
  int conv_context_{6};
  int sample_rate_{16000};
  int max_audio_second_{15};

  std::map<int, std::string> vocab_;
  int output_num_{-1};

  std::string hpy_;

  std::vector<std::vector<float>> feats_;

  int acc_feats_num_;
  int feats_offset_{0};

  int sample_num_{0};

  torch::jit::script::Module asr_model_;
  bool inited_;
};

#endif // STREAMING_ASR_ON_DEVICE_H_