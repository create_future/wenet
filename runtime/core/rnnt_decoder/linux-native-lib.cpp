/*
 * @Author: gaoxinglong
 * @Date: 2022-11-16 14:24:20
 * @LastEditTime: 2022-11-30 16:35:33
 * @LastEditors: gaoxinglong
 */
#include "streaming_asr_on_device.h"
#include "utils.h"
#include <cstdint>
// #include <jni.h>
#include <string>

StreamingAsrConfig asr_config;
StreamingAsrOnDevice asr(asr_config);

#ifdef __cplusplus
extern "C" {
#endif

// extern "C" JNIEXPORT jint JNICALL
int32_t
Java_com_xdf_deviceasr_StreamingAsrOnDevice_init(const char *modelPath,
                                                 const char *vocabPath) {
  std::string model_path(modelPath);
  std::string vocab_path(vocabPath);
  asr.init();
  return 0;
}

const char *Java_com_xdf_deviceasr_StreamingAsrOnDevice_onRecieve(
    const std::vector<float> &audioBuffer, bool isEnd, int len) {

  float *data = new float[len];
  memcpy((float *)data, audioBuffer.data(), sizeof(float) * audioBuffer.size());

  std::string res = asr.onRecieve(data, len, isEnd);
  return res.c_str();
}

void Java_com_xdf_deviceasr_StreamingAsrOnDevice_reset() { asr.reset(); }

#ifdef __cplusplus
}
#endif
