/*
 * @Author: gaoxinglong
 * @Date: 2022-11-24 15:10:30
 * @LastEditTime: 2022-11-30 20:06:06
 * @LastEditors: gaoxinglong
 */

#include "utils.h"
#include <iostream>
#include <stdint.h>
#include <vector>

using namespace std;
using namespace wenet;

class OnlineStreaming {
public:
  explicit OnlineStreaming(const wenet::StreamingAsrConfig &asr_config);
  virtual ~OnlineStreaming();
  void ReceiveNewChunk(const std::vector<float> &new_chunk,
                       bool is_last = false);
  int32_t Offset() const { return offset_; }
  int32_t AccumFrames() const { return accum_frames_; }
  void Reset() {
    offset_ = 0;
    accum_frames_ = 0;
    frame_buffer_.clear();
    cnn_lookahead_ = false;
    cnn_lookback_ = false;
  }
  void NextBlock() { offset_ += current_context_length_; }
  int32_t Factor() const { return factor_; }
  int32_t ConvContxt() const { return conv_context_length_; }
  void GetBlock(std::vector<float> &block_output);
  bool LookAhead() const { return cnn_lookahead_; }
  bool LookBackward() const { return cnn_lookback_; }
  int32_t CurrentBlockFrameSize() const { return temp_block_frame_size_; }
  int32_t CurrentBlockSize() const { return current_context_length_; }
  //   int32_t RightBlockSize() const { return right_context_length_; }
  int32_t GetTempStart() const { return temp_start_; }
  int32_t GetTempEnd() const { return temp_end_; }
  int32_t GetDim() const { return input_dim_; }
  float GetFeatureValue(int32_t row, int32_t col) const {
    return frame_buffer_[row * input_dim_ + col];
  }

protected:
  void RemoveOldBlock(){};

private:
  bool input_finished_{false};
  int input_dim_{80};
  bool cnn_lookback_{false};
  bool cnn_lookahead_{false};
  int32_t temp_start_{-1};
  int32_t temp_end_{-1};
  int32_t temp_block_frame_size_{-1};
  int32_t conv_context_length_{6};
  int32_t left_context_length_{0};
  int32_t right_context_length_{40};
  int32_t current_context_length_{40};
  int32_t factor_{6}; // the same as conv_context_length_
  int32_t offset_{0};
  int32_t accum_frames_{0};
  std::vector<float> frame_buffer_{};
};