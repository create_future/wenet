/*
 * @Author: gaoxinglong
 * @Date: 2022-11-16 14:24:20
 * @LastEditTime: 2022-11-16 14:27:05
 * @LastEditors: gaoxinglong
 */
#include "streaming_asr_on_device.h"
#include <jni.h>
#include <string>

StreamingAsrConfig asr_config;
StreamingAsrOnDevice asr(asr_config);

extern "C" JNIEXPORT jint JNICALL
Java_com_xdf_deviceasr_StreamingAsrOnDevice_init(JNIEnv *env,
                                                 jobject /* this */,
                                                 jstring modelPath,
                                                 jstring vocabPath) {
  const char *cModelPath = env->GetStringUTFChars(modelPath, 0);
  const char *cVocabPath = env->GetStringUTFChars(vocabPath, 0);
  std::string model_path(cModelPath);
  std::string vocab_path(cVocabPath);
  asr.init(model_path, vocab_path);
  return 0;
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_xdf_deviceasr_StreamingAsrOnDevice_onRecieve(JNIEnv *env,
                                                      jobject /* this */,
                                                      jfloatArray audioBuffer,
                                                      jboolean isEnd,
                                                      jint len) {

  float *data = new float[len];
  env->GetFloatArrayRegion(audioBuffer, 0, len, data);

  std::string res = asr.onRecieve(data, len, isEnd);
  return env->NewStringUTF(res.c_str());
}

extern "C" JNIEXPORT void JNICALL
Java_com_xdf_deviceasr_StreamingAsrOnDevice_reset(JNIEnv *env,
                                                  jobject /* this */) {
  asr.reset();
}