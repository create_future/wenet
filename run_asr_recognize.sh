
###
 # @Author: gaoxinglong
 # @Date: 2022-11-29 14:54:48
 # @LastEditTime: 2023-01-29 12:48:28
 # @LastEditors: gaoxinglong && gaoxinglong9999@sina.cn
### 

export LD_LIBRARY_PATH=/home/gaoxinglong/env/bin/anaconda3/envs/wenet/lib/python3.8/site-packages/nvidia/cublas/lib/:${LD_LIBRARY_PATH}
# env_root=/home/gaoxinglong/env/tools/wenet/examples/aishell2/rnnt
cd ${env_root}
exp_root=/home/gaoxinglong/env/tools/wenet/examples/multi_cn/s0
cmd_tool=/home/gaoxinglong/env/tools/wenet/stream_asr_recognize.py
check_point=${env_root}/exp/conformer_rnnt_small/21.pt
result_file=${env_root}/exp/conformer_rnnt_small/21.result
test_file=${env_root}/data/test/data.list
dict_file=${env_root}/data/dict/lang_pinyin_en.txt
decode_modes="ctc_prefix_beam_search"
# decode_modes="rnnt_beam_search_stream"
# decode_modes="rnnt_beam_search"
decode_configs=${env_root}/exp/conformer_rnnt_small/train.yaml


# 
python ${cmd_tool} --config  ${decode_configs}  --data_type raw \
--dict ${dict_file}   --mode  $decode_modes  \
--test_data   ${test_file}    \
--checkpoint  ${check_point}  --result_file  ${result_file}   --decoding_chunk_size 16  \
